package api

import (
	"bytes"
	"context"
	"encoding/json"
	"io"
	"net/http"
)

const ServerListEndpoint = "https://serverlist.piaservers.net/vpninfo/servers/v6"

type ServerList struct {
	Groups  map[string][]Group `json:"groups"`
	Regions []Region           `json:"regions"`
}

type Group struct {
	Name  string   `json:"name"`
	Ports []uint16 `json:"ports"`
}

type Region struct {
	ID          string              `json:"id"`
	Name        string              `json:"name"`
	Country     string              `json:"country"`
	AutoRegion  bool                `json:"auto_region"`
	DNS         string              `json:"dns"`
	PortForward bool                `json:"port_forward"`
	Geo         bool                `json:"geo"`
	Servers     map[string][]Server `json:"servers"`
}

type Server struct {
	IP         string `json:"ip"`
	CommonName string `json:"cn"`
}

func GetServerList(ctx context.Context) (l ServerList, err error) {
	req, err := http.NewRequestWithContext(ctx, http.MethodGet, ServerListEndpoint, nil)

	if err != nil {
		return
	}

	res, err := http.DefaultClient.Do(req)

	if err != nil {
		return
	}

	defer res.Body.Close()

	body, err := io.ReadAll(res.Body)

	if err != nil {
		return
	}

	err = json.NewDecoder(bytes.NewReader(body)).Decode(&l)

	return
}
