package api

import (
	"fmt"
	"net/http"
	"strings"
)

const (
	statusCodeError = "unexpected status code"
)

type StatusCodeError struct {
	StatusCode int
}

func (sce *StatusCodeError) Error() string { return sce.String() }

func (sce *StatusCodeError) String() string {
	return fmt.Sprintf(
		"%s: %d (%s)",
		statusCodeError,
		sce.StatusCode,
		http.StatusText(sce.StatusCode),
	)
}

func (sce *StatusCodeError) Is(err error) bool {
	return strings.HasPrefix(err.Error(), statusCodeError)
}
