package api

import (
	"bytes"
	"context"
	"encoding/json"
	"net/http"
)

const (
	tokenEndpointV2 = "https://www.privateinternetaccess.com/api/client/v2/token"
	tokenEndpointV1 = "https://privateinternetaccess.com/gtoken/generateToken"
)

type Token string

type tokenRequest struct {
	Username string `json:"username"`
	Password string `json:"password"`
}

type tokenResponse struct {
	Token *Token `json:"token"`
}

func GetToken(ctx context.Context, username, password string) (*Token, error) {
	token, err := GetTokenV1(ctx, username, password)
	if _, ok := err.(*StatusCodeError); ok {
		token, err = GetTokenV2(ctx, username, password)
	}
	if err != nil {
		return nil, err
	}
	return token, nil
}

func GetTokenV2(ctx context.Context, username, password string) (*Token, error) {
	buf := bytes.NewBuffer(nil)
	err := json.NewEncoder(buf).Encode(tokenRequest{
		Username: username,
		Password: password,
	})
	if err != nil {
		return nil, err
	}

	req, err := http.NewRequestWithContext(ctx, http.MethodPost, tokenEndpointV2, buf)
	if err != nil {
		return nil, err
	}
	req.Header.Set("Content-Type", "application/json")

	res, err := http.DefaultClient.Do(req)
	if err != nil {
		return nil, err
	}
	defer res.Body.Close()

	if res.StatusCode < 200 || res.StatusCode >= 300 {
		return nil, &StatusCodeError{StatusCode: res.StatusCode}
	}

	var response = &tokenResponse{}
	if err = json.NewDecoder(res.Body).Decode(response); err != nil {
		return nil, err
	}

	return response.Token, nil
}

func GetTokenV1(ctx context.Context, username, password string) (*Token, error) {
	req, err := http.NewRequestWithContext(ctx, http.MethodPost, tokenEndpointV1, nil)
	if err != nil {
		return nil, err
	}
	req.SetBasicAuth(username, password)

	res, err := http.DefaultClient.Do(req)
	if err != nil {
		return nil, err
	}
	defer res.Body.Close()

	if res.StatusCode < 200 || res.StatusCode >= 300 {
		return nil, &StatusCodeError{StatusCode: res.StatusCode}
	}

	var response = &tokenResponse{}
	if err = json.NewDecoder(res.Body).Decode(response); err != nil {
		return nil, err
	}

	return response.Token, nil
}
