# PIA No Lag Warzone

This tool generates a [*Wireguard*](https://www.wireguard.com/) configuration for the selected country with pre-defined routes that will mask your location for Call of Duty Warzone whilst maintaining your regular latency as you're cleaning the lobby 😎.

If you already own an Private Internet Access subscription, you don't have to buy another subscription from https://nolagvpn.com/, as all they're doing is providing an OpenVPN configuration with the routes provided.

## Requirements

- An active subscription from Private Internet Access.
- Wireguard client installed, one can be downloaded from here 'https://www.wireguard.com/install/'.

## Installation

A simple `go build -v` will build the application.

## Usage

- Open a terminal at the working directory of this application.
- Execute `./pia-nolag-warzone --username <username> --password <password>`.
- A prompt is provided with the available servers.
- Add the configuration to the Wireguard client.
- Activate the connection.
- Hop into Warzone and ensrue your Geographical Location is set to the desired country.

# Credits
- https://nolagvpn.com/ : for blatantly having the routes in their OpenVPN configurations and provided me with this idea.