package route

import (
	"bufio"
	"net"
	"os"
	"regexp"
	"strings"
)

var (
	openvpnRoute = regexp.MustCompile(`route (.+) (.+) ?`)
)

func ParseRoutes(routes string) (ipnet []net.IPNet) {
	fs, err := os.Open(routes)

	if err != nil {
		return
	}

	defer fs.Close()

	reader := bufio.NewReader(fs)

	for {
		line, _, err := reader.ReadLine()

		if err != nil {
			return ipnet
		}

		if openvpnRoute.MatchString(string(line)) {
			matches := openvpnRoute.FindStringSubmatch(string(line))

			ipnet = append(ipnet, net.IPNet{
				IP:   net.ParseIP(strings.TrimSpace(matches[1])),
				Mask: net.IPMask(net.ParseIP(strings.TrimSpace(matches[2]))),
			})
		}
	}
}
