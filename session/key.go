package session

import "golang.zx2c4.com/wireguard/wgctrl/wgtypes"

type PrivateKey wgtypes.Key

func NewKey() (pk PrivateKey, err error) {
	wpk, err := wgtypes.GeneratePrivateKey()
	pk = PrivateKey(wpk)
	return
}

func (pk PrivateKey) Key() wgtypes.Key { return wgtypes.Key(pk) }

func (pk PrivateKey) String() string {
	return wgtypes.Key(pk).String()
}

func (pk PrivateKey) PublicKey() PublicKey {
	return PublicKey(wgtypes.Key(pk).PublicKey())
}

type PublicKey wgtypes.Key

func (pk PublicKey) Key() wgtypes.Key { return wgtypes.Key(pk) }

func (pk *PublicKey) UnmarshalText(text []byte) (err error) {
	wk, err := wgtypes.ParseKey(string(text))
	*pk = PublicKey(wk)
	return
}

func (pk PublicKey) String() string {
	return wgtypes.Key(pk).String()
}
