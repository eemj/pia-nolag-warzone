package session

import (
	"context"
	"crypto/tls"
	"encoding/json"
	"fmt"
	"net"
	"net/http"
	"net/url"
	"strings"

	"gitlab.com/eemj/pia-nolag-warzone/api"
	"golang.zx2c4.com/wireguard/wgctrl/wgtypes"
)

type Session struct {
	Status          string      `json:"status"`
	ServerPublicKey PublicKey   `json:"server_key"`
	ServerPort      uint16      `json:"server_port"`
	ServerAddr      net.UDPAddr `json:"-"`
	ServerIP        net.IP      `json:"server_ip"`
	ServerVIP       net.IP      `json:"server_vip"`
	PeerIP          net.IP      `json:"-"`
	RawPeerIP       string      `json:"peer_ip"`
	PeerPublicKey   PublicKey   `json:"peer_pubkey"`
	DNSServers      []net.IP    `json:"dns_servers"`
}

func (s Server) AddKey(ctx context.Context, username, password string, key PublicKey) (sn Session, err error) {
	token, err := api.GetToken(ctx, username, password)
	if err != nil {
		return
	}

	req, err := http.NewRequestWithContext(
		ctx,
		http.MethodGet,
		(&url.URL{
			Scheme: "https",
			Host:   s.Addr.String(),
			Path:   "addKey",
			RawQuery: url.Values{
				"pubkey": []string{wgtypes.Key(key).String()},
				"pt":     []string{string(*token)},
			}.Encode(),
		}).String(),
		nil,
	)
	req.Host = s.CommonName

	cli := http.Client{
		Transport: &http.Transport{
			DialTLS: func(network, addr string) (net.Conn, error) {
				return tls.Dial(network, addr, &tls.Config{
					InsecureSkipVerify: true,
					ServerName:         s.CommonName,
				})
			},
		},
	}

	res, err := cli.Do(req)

	if err != nil {
		return
	}

	defer res.Body.Close()

	if res.StatusCode != http.StatusOK {
		err = fmt.Errorf("expected '200' but got '%v'", res.StatusCode)
		return
	}

	if err = json.NewDecoder(res.Body).Decode(&sn); err != nil {
		return
	}

	sn.ServerAddr = net.UDPAddr{
		IP:   sn.ServerIP,
		Port: int(sn.ServerPort),
	}

	sn.PeerIP = net.ParseIP(strings.ReplaceAll(sn.RawPeerIP, "/32", ""))

	if err == nil && sn.Status != "OK" {
		err = fmt.Errorf("error from /addKey: %s", sn.Status)
	}

	return
}
