package session

import (
	"net"
	"strconv"
	"strings"

	"golang.zx2c4.com/wireguard/wgctrl/wgtypes"
)

type Interface struct {
	PrivateKey wgtypes.Key
	Address    net.IP
	DNS        []net.IP
}

type Peer struct {
	PublicKey           wgtypes.Key
	Endpoint            net.UDPAddr
	AllowedIPs          []net.IPNet
	PersistentKeepalive int
}

type Config struct {
	Interface Interface
	Peers     []Peer
}

func (c Config) String() string {
	sb := new(strings.Builder)

	sb.WriteString("[Interface]\n")
	sb.WriteString("PrivateKey = ")
	sb.WriteString(c.Interface.PrivateKey.String())
	sb.WriteString("\n")
	sb.WriteString("Address = ")
	sb.WriteString(c.Interface.Address.String())
	sb.WriteString("\n")

	if len(c.Interface.DNS) > 0 {
		sb.WriteString("DNS = ")

		for index, dns := range c.Interface.DNS {
			sb.WriteString(dns.String())

			if index < (len(c.Interface.DNS) - 1) {
				sb.WriteString(", ")
			}
		}

		sb.WriteString("\n")
	}

	for _, peer := range c.Peers {
		sb.WriteString("\n")
		sb.WriteString("[Peer]\n")
		sb.WriteString("PublicKey = ")
		sb.WriteString(peer.PublicKey.String())
		sb.WriteString("\n")
		sb.WriteString("Endpoint = ")
		sb.WriteString(peer.Endpoint.String())
		sb.WriteString("\n")
		sb.WriteString("AllowedIPs = ")

		if len(peer.AllowedIPs) > 0 {
			for index, allowedIP := range peer.AllowedIPs {
				sb.WriteString(allowedIP.String())

				if index < (len(peer.AllowedIPs) - 1) {
					sb.WriteString(", ")
				}
			}
		} else {
			sb.WriteString("0.0.0.0/0")
		}

		sb.WriteString("\n")
		sb.WriteString("PersistentKeepalive = ")
		sb.WriteString(strconv.Itoa(peer.PersistentKeepalive))
		sb.WriteString("\n")
	}

	return sb.String()
}
