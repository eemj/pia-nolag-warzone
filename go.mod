module gitlab.com/eemj/pia-nolag-warzone

go 1.16

require (
	github.com/manifoldco/promptui v0.8.0
	golang.org/x/crypto v0.0.0-20210513164829-c07d793c2f9a // indirect
	golang.zx2c4.com/wireguard/wgctrl v0.0.0-20210506160403-92e472f520a5
)
