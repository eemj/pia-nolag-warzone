package main

import (
	"context"
	"errors"
	"flag"
	"fmt"
	"io"
	"log"
	"os"
	"sort"
	"strings"

	"github.com/manifoldco/promptui"
	"gitlab.com/eemj/pia-nolag-warzone/route"
	"gitlab.com/eemj/pia-nolag-warzone/session"
)

var (
	username = ""
	password = ""
	ovpn     = ""
)

func parseFlags() error {
	flag.StringVar(&username, "username", "", "username for https://www.privateinternetaccess.com")
	flag.StringVar(&password, "password", "", "password for https://www.privateinternetaccess.com")
	flag.StringVar(&ovpn, "ovpn", "", "ovpn file location")

	flag.Parse()

	if len(strings.TrimSpace(username)) == 0 {
		return errors.New("`username` flag is required")
	}

	if len(strings.TrimSpace(password)) == 0 {
		return errors.New("`password` flag is required")
	}

	if len(strings.TrimSpace(ovpn)) == 0 {
		return errors.New("`ovpn` flag is required")
	}

	return nil
}

func main() {
	if err := parseFlags(); err != nil {
		log.Fatal(err)
	}

	ctx, cancel := context.WithCancel(context.Background())

	defer cancel()

	serverList, err := session.GetServers(ctx)

	if err != nil {
		log.Fatal(err)
	}

	countryKeys := make([]string, 0)
	countryServers := make(map[string][]session.Server)

	for _, list := range serverList {
		for _, server := range list {
			if _, exists := countryServers[server.Country]; !exists {
				countryServers[server.Country] = make([]session.Server, 0)
				countryKeys = append(countryKeys, server.Country)
			}

			countryServers[server.Country] = append(
				countryServers[server.Country],
				server,
			)
		}
	}

	sort.Strings(countryKeys)

	prompt := &promptui.Select{
		Items:             countryKeys,
		Size:              8,
		StartInSearchMode: true,
		Searcher: func(input string, index int) bool {
			return strings.Contains(
				strings.TrimSpace(strings.ToLower(countryKeys[index])),
				strings.TrimSpace(strings.ToLower(input)),
			)
		},
	}

	_, result, err := prompt.Run()

	if err != nil {
		log.Fatal(err)
	}

	server := countryServers[result][0]

	privateKey, err := session.NewKey()

	if err != nil {
		log.Fatal(err)
	}

	publicKey := privateKey.PublicKey()

	sess, err := server.AddKey(ctx, username, password, publicKey)

	if err != nil {
		log.Fatal(err)
	}

	routes := route.ParseRoutes(ovpn)

	cfg := &session.Config{
		Interface: session.Interface{
			PrivateKey: privateKey.Key(),
			Address:    sess.PeerIP,
			DNS:        sess.DNSServers,
		},
		Peers: []session.Peer{{
			PublicKey:           sess.ServerPublicKey.Key(),
			Endpoint:            sess.ServerAddr,
			AllowedIPs:          routes,
			PersistentKeepalive: 25,
		}},
	}

	f, err := os.CreateTemp(
		".",
		fmt.Sprintf("wg0-%s-*.conf", strings.ToLower(server.Country)),
	)

	if err != nil {
		log.Fatal(err)
	}

	defer f.Close()

	if _, err = io.Copy(f, strings.NewReader(cfg.String())); err != nil {
		log.Fatal(err)
	}
}
